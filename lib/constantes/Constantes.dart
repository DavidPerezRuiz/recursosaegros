
class Constantes{

  static const String wsMedico = "http://127.0.0.1/AEGROS/wsMedico.php";
  static const String wsPaciente = "http://127.0.0.1/AEGROS/wsPaciente.php";
  static const String wsCita = "http://127.0.0.1/AEGROS/wsCita.php";
  static const String wsHorario = "http://127.0.0.1/AEGROS/wsHorario.php";
  static const String wsResultado = "http://127.0.0.1/AEGROS/wsResultado.php";
  static const String wsEspecialidad = "http://127.0.0.1/AEGROS/wsEspecialidad.php";

}