
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:recursos_aegros/constantes/Constantes.dart';

class Horario {

  static final Uri uri = Uri.parse(Constantes.wsHorario);

  int? id_horario;
  String? dias;
  String? hora_inicio;
  String? hora_fin;
  int? id_medico;

  Horario({
    this.id_horario,
    this.dias,
    this.hora_inicio,
    this.hora_fin,
    this.id_medico,
  });

  static Horario decodedJSONaHorario(var informacionHorario) {

    int? id_horario = (informacionHorario['id_horario'] == null ? null : int.parse(informacionHorario['id_horario']));
    int? id_medico = (informacionHorario['id_medico'] == null ? null : int.parse(informacionHorario['id_medico']));

    return Horario(
        id_horario: id_horario,
        id_medico: id_medico,
        dias: informacionHorario['dias'],
        hora_inicio: informacionHorario['hora_inicio'],
        hora_fin: informacionHorario['hora_fin'],
    );
  }

  /**
   * Devuelve el horario buscado con el id_medico.
   *
   * El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se han devuelto citas o no.
   *    * horario: Horario - solo existirá cuando la petición sea correcta, contiene las filtradas.
   */
  static Future<Map<String, dynamic>> buscar(int id_medico) async{

    Response response = await http.post(uri, body: jsonEncode({
      "accion": "buscar",
      "id_medico":id_medico
    }));

    int respuesta_http = response.statusCode;
    Map<String,dynamic> body = jsonDecode(response.body);

    Map<String,dynamic> data = body['data'];

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    if(body['success']==false) {
      respuesta.addAll(
          {
            "success" : false
          }
      );
    } else {


      respuesta.addAll(
          {
            "success" : true,
            "horario" : decodedJSONaHorario(data)
          }
      );
    }

    return respuesta;
  }


  @override
  String toString() {
    return "ID_Medico : $id_medico\n";
  }

}