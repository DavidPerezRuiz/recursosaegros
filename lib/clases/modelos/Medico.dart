import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:recursos_aegros/clases/modelos/Usuario.dart';
import 'package:recursos_aegros/constantes/Constantes.dart';

class Medico extends Usuario{

  static final Uri uri = Uri.parse(Constantes.wsMedico);

  int? id_medico;
  String? nombre;
  String? apellido_1;
  String? apellido_2;
  String? dni;
  String? telefono;
  String? direccion;
  int? id_especialidad;
  String? jwt;

  Medico({this.id_medico,
    this.id_especialidad,
    this.nombre,
    this.dni,
    this.apellido_1,
    this.apellido_2,
    this.telefono,
    this.direccion,
    int? id_usuario,
    String? correo,
    String? contrasena,
    String? rol,
    String? jwt
  }) : super(
    id_usuario: id_usuario,
    correo: correo,
    contrasena: contrasena,
    rol: rol,
    jwt: jwt
  );

  static Medico decodedJSONaMedico(var informacionMedico){

    // Ya que id_especialidad puede ser null parsearlo a int provocará una excepción. Se comprueba aquí si lo es
    // y en caso de no serlo realizara el parseo.
    int? id_especialidad_data = (informacionMedico['id_especialidad'] == null ? null : int.parse(informacionMedico['id_especialidad']));

    return Medico(
        id_medico: int.parse(informacionMedico['id_medico']),
        dni: informacionMedico['dni'],
        nombre: informacionMedico['nombre'],
        apellido_1: informacionMedico['apellido_1'],
        apellido_2: informacionMedico['apellido_2'],
        telefono: informacionMedico['telefono'],
        direccion: informacionMedico['direccion'],
        id_especialidad: id_especialidad_data,
        id_usuario: int.parse(informacionMedico['id_usuario']),
        correo: informacionMedico['correo'],
        contrasena: informacionMedico['contrasena'],
        rol: informacionMedico['rol'],
        jwt: informacionMedico['jwt'],
    );
  }

  Map<String,dynamic> aJSON(){
    return {
      "id_medico":id_medico,
      "dni":dni,
      "nombre":nombre,
      "apellido_1":apellido_1,
      "apellido_2":apellido_2,
      "telefono":telefono,
      "direccion":direccion,
      "especialidad":id_especialidad,
      "id_usuario":id_usuario,
      "correo":correo,
      "contrasena":contrasena

    };
  }

  /**
   * Devuelve todos los datos de un medico segun el id_medico. El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se ha devuelto un medico o no.
   *    * medico: Medico - solo existirá cuando la petición sea correcta, contiene el médico solicitado.
   */
  static Future<Map<String, dynamic>> buscar(int id_usuario) async{

    Response response = await http.post(uri, body: jsonEncode({
      "accion": "buscar",
      "id_usuario":id_usuario
    }));

    int respuesta_http = response.statusCode;
    Map<String,dynamic> body = jsonDecode(response.body);

    Map<String,dynamic> informacionMedico = body['data'];

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    if(body['success']==false) {
      respuesta.addAll(
        {
          "success" : false
        }
      );
    } else {


      respuesta.addAll(
          {
            "success" : true,
            "medico" : decodedJSONaMedico(informacionMedico)
          }
      );
    }

    return respuesta;

  }

  /**
   * Devuelve todos los datos de los medicos filtrados. El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se han devuelto medicos o no.
   *    * medicos: List<Medico> - solo existirá cuando la petición sea correcta, contiene los médicos filtrados.
   */
  static Future<Map<String, dynamic>> filtrar(Map<String,dynamic>? filtros, int? numPag, int? numRegistros) async {

    List<Medico> listaMedicos = [];

    numPag ??= 1;
    numRegistros ??= 5;

    Map<String, dynamic> bodyPeticion = {
      "accion":"filtrar",
      "pag":numPag,
      "numRegistros":numRegistros
    };

    if(filtros!=null){
      bodyPeticion.addAll(filtros);
    }

    Response response = await http.post(uri, body: jsonEncode(bodyPeticion));

    int respuesta_http = response.statusCode;

    // Cuerpo de la petición
    Map<String,dynamic> body = jsonDecode(response.body);

    // Datos de los medicos
    Map<String,dynamic> data = body['data'];

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    if(body['success']==false) {
      respuesta.addAll(
          {
            "success" : false
          }
      );
    } else {

      int totalRegistros = int.parse(data['totalRegistros']);
      data.removeWhere((String key, dynamic value) => key == "totalRegistros");

      data.forEach((key, value) {
        listaMedicos.add(decodedJSONaMedico(data[key]));
      });

      respuesta.addAll(
          {
            "success" : true,
            "totalRegistros":totalRegistros,
            "medicos" : listaMedicos
          }
      );

    }
    return respuesta;

  }

  @override
  String toString() {
    return "ID_Medico : $id_medico\n"
        "DNI: $dni\n"
        "Nombre: $nombre\n"
        "Apellido_1: $apellido_1\n"
        "Apellido_2: $apellido_2\n"
        "Telefono: $telefono\n"
        "Direccion: $direccion\n"
        "Especialidad: $id_especialidad\n"
        "ID_Usuario: $id_usuario\n"
        "Correo: $correo\n";
  }


}