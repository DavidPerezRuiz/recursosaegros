
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:recursos_aegros/constantes/Constantes.dart';

class Especialidad {

  static final Uri uri = Uri.parse(Constantes.wsEspecialidad);

  int? id_especialidad;
  String? titulo;
  String? descripcion;

  Especialidad({
    this.id_especialidad,
    this.titulo,
    this.descripcion
  });

  static Especialidad decodedJSONaEspecialidad(var informacionEspecialidad) {

    int? id_especialidad = (informacionEspecialidad['id_especialidad'] == null ? null : int.parse(informacionEspecialidad['id_especialidad']));

    return Especialidad(
      id_especialidad: id_especialidad,
      titulo: informacionEspecialidad['titulo'],
      descripcion: informacionEspecialidad['descripcion']
    );
  }

  /**
   * Devuelve todas las especialidades existentes.
   *
   * El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se han devuelto especialidades o no.
   *    * especialidades: List<Especialidad> - contiene la lista de especialidades
   */
  static Future<Map<String, dynamic>> listar() async{

    Response response = await http.post(uri, body: jsonEncode({
      "accion": "listar"
    }));

    int respuesta_http = response.statusCode;
    Map<String,dynamic> body = jsonDecode(response.body);

    Map<String,dynamic> data = body['data'];

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    if(data['success']==false) {
      respuesta.addAll(
          {
            "success" : false
          }
      );
    } else {

      int totalRegistros = int.parse(data['totalRegistros']);
      data.removeWhere((String key, dynamic value) => key == "totalRegistros");

      List<Especialidad> listaEspecialidades = [];

      data.forEach((key, value) {
        listaEspecialidades.add(decodedJSONaEspecialidad(data[key]));
      });

      respuesta.addAll(
          {
            "success" : true,
            "totalRegistros":totalRegistros,
            "especialidades" : listaEspecialidades
          }
      );
    }

    return respuesta;
  }

}