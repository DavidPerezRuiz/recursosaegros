import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:recursos_aegros/clases/modelos/Clases.dart';

abstract class Usuario {
  int? id_usuario;
  String? correo;
  String? contrasena;
  String? jwt;
  String? rol;

  static Uri uri = Uri.parse("http://127.0.0.1/AEGROS/wsUsuario.php");

  Usuario({
    this.id_usuario,
    this.correo,
    this.contrasena,
    this.jwt,
    this.rol
  });

  /**
   * Inicia sesion con el correo y contraseña indicados. Si sale bien los datos del usuario se guardaran
   * en Sesion.usuario
   */
  static Future<bool> login(String correo, String contrasena) async {

    var response = await http.post(uri, body: jsonEncode({
      "accion": "login",
      "correo": correo,
      "contrasena": contrasena
    }));
    print(response.body);
    var respuesta = jsonDecode(response.body);

    Map<String,dynamic> data = respuesta['data'];
    bool success = respuesta['success'];

    // http_response

    if(success){
      switch (data['rol'].toString()){
        case "Medico":
          Sesion.usuario = Medico.decodedJSONaMedico(data);
          break;
        case "Paciente":
          Sesion.usuario = Paciente.decodedJSONaPaciente(data);
          break;
      }

    }
    return success;

  }

  /**
   * Comprueba la validez de la sesión con el JWT que tenga el usuario actualmente.*--
   */
  Future<bool> comprobarSesionValida() async {

    Uri uri = Uri.parse("http://127.0.0.1/AEGROS/wsUsuario.php");

    var response = await http.post(uri, body: jsonEncode({
      "accion": "validar",
      "jwt": jwt,
      "rol": rol
    }));

    Map<String,dynamic> data = jsonDecode(response.body)['data'];

    return data['success'];

  }

}