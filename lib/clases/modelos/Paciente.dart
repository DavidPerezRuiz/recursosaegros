import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:recursos_aegros/clases/modelos/Clases.dart';
import 'package:recursos_aegros/clases/modelos/Usuario.dart';
import 'package:recursos_aegros/constantes/Constantes.dart';

class Paciente extends Usuario{

  static final Uri uri = Uri.parse(Constantes.wsPaciente);

  int? id_paciente;
  String? dni;
  String? sip;
  String? nombre;
  String? apellido_1;
  String? apellido_2;
  String? telefono;
  String? direccion;

  Paciente({
    required this.id_paciente,
    this.dni,
    this.sip,
    this.nombre,
    this.apellido_1,
    this.apellido_2,
    this.telefono,
    this.direccion,

    int? id_usuario,
    String? correo,
    String? contrasena,
    String? rol,
    String? jwt
  }) : super(
    id_usuario: id_usuario,
    correo: correo,
    contrasena: contrasena,
    rol: rol,
    jwt: jwt
  );

  static Paciente decodedJSONaPaciente(var informacionPaciente){

    return Paciente(
        id_paciente: int.parse(informacionPaciente['id_paciente']),
        dni: informacionPaciente['dni'],
        sip: informacionPaciente['sip'],
        nombre: informacionPaciente['nombre'],
        apellido_1: informacionPaciente['apellido_1'],
        apellido_2: informacionPaciente['apellido_2'],
        telefono: informacionPaciente['telefono'],
        direccion: informacionPaciente['direccion'],
        id_usuario: int.parse(informacionPaciente['id_usuario']),
        correo: informacionPaciente['correo'],
        contrasena: informacionPaciente['contrasena'],
        rol: informacionPaciente['rol'],
        jwt: informacionPaciente['jwt']
    );
  }

  Map<String,dynamic> aJSON(){
    //hay que incluir aqui id_paciente, id_usuario, apellido_2 y token
    return {
      "id_paciente":id_paciente,
      "dni":dni,
      "sip":sip,
      "nombre":nombre,
      "apellido_1":apellido_1,
      "apellido_2":apellido_2,
      "telefono":telefono,
      "direccion":direccion,
      "id_usuario":id_usuario,
      "correo":correo,
      "contrasena":contrasena,
      "jwt":jwt

    };
  }

  /**
   * Registra un paciente.
   *
   * <br>El objeto tiene que tener indicado los siguientes parametros:
   *  * dni
   *  * sip
   *  * nombre
   *  * apellido_1
   *  * telefono
   *  * direccion
   *  * correo
   *  * contrasena
   *
   * El mapa devuelto contiene tres partes:
   *  * codigo: int - Codigo de respuesta HTTP.
   *  * msg: String - Resumen de la operación.
   *  * success: bool - Indica si se han devuelto medicos o no.
   */
  Future<Map<String, dynamic>> insertar() async {

    Map<String,dynamic> body = {"accion":"insertar"};
    body.addAll(aJSON());

    var response = await http.post(uri, body: json.encode(body));
    var data = jsonDecode(response.body);

    String seHaInsertado = data['success'].toString();
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "success": seHaInsertado,
      "msg": msg
    };

    return respuesta;
  }

  /**
   * Modifica el paciente. El mapa devuelto contiene tres partes:
   *  * codigo: int - Codigo de respuesta HTTP.
   *  * msg: String - Resumen de la operación.
   *  * success: bool - Indica si se ha modificado el paciente o no.
   */
  Future<Map<String, dynamic>> modificar() async {

    Map<String,dynamic> body = {
      "accion":"modificar",
    };
    body.addAll(aJSON());

    var response = await http.post(uri, body: json.encode(body));
    var data = jsonDecode(response.body);

    String seHaInsertado = data['success'].toString();
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    print(data);

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "success": seHaInsertado,
      "msg": msg
    };

    return respuesta;
  }

  @override
  String toString() {
    return "ID_Paciente : $id_paciente\n"
        "DNI: $dni\n"
        "SIP: $sip\n"
        "Nombre: $nombre\n"
        "Apellido_1: $apellido_1\n"
        "Apellido_2: $apellido_2\n"
        "Telefono: $telefono\n"
        "Direccion: $direccion\n"
        "ID_Usuario: $id_usuario\n"
        "Correo: $correo\n";
  }



}