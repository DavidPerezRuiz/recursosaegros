
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:recursos_aegros/clases/Sesion.dart';
import 'package:recursos_aegros/constantes/Constantes.dart';

class Cita {

  static final Uri uri = Uri.parse(Constantes.wsCita);

  int? id_cita;
  int? id_paciente;
  int? id_medico;
  int? id_resultado;
  String? dia;
  String? hora;

  Cita({
    this.id_cita,
    this.id_paciente,
    this.id_medico,
    this.id_resultado,
    this.dia,
    this.hora
  });

  static Cita decodedJSONaCita(var informacionCita) {

    int? id_cita = (informacionCita['id_cita'] == null ? null : int.parse(informacionCita['id_cita']));
    int? id_paciente = (informacionCita['id_paciente'] == null ? null : int.parse(informacionCita['id_paciente']));
    int? id_medico = (informacionCita['id_medico'] == null ? null : int.parse(informacionCita['id_medico']));
    int? id_resultado = (informacionCita['id_resultado'] == null ? null : int.parse(informacionCita['id_resultado']));

    return Cita(
      id_cita: id_cita,
      id_paciente: id_paciente,
      id_medico: id_medico,
      id_resultado: id_resultado,
      dia: informacionCita['dia'],
      hora: informacionCita['hora'],
    );
  }

  Map<String,dynamic> aJSON(){
    return {
      "id_cita":id_cita,
      "id_paciente":id_paciente,
      "id_medico":id_medico,
      "id_resultado":id_resultado,
      "dia":dia,
      "hora":hora,
      "jwt":Sesion.usuario.jwt
    };
  }

  /**
   * Devuelve todos los datos de la cita indicada con id_cita siempre que sea participe en esta.
   *
   * El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se han devuelto citas o no.
   *    * cita: Cita - solo existirá cuando la petición sea correcta, contiene el objeto de la cita.
   */
  static Future<Map<String, dynamic>> buscar(int id_cita) async {

    Response response = await http.post(uri, body: jsonEncode({
      "accion": "buscar",
      "id_cita": id_cita,
      "id_usuario": Sesion.usuario.id_usuario,
      "jwt":Sesion.usuario.jwt
    }));

    int respuesta_http = response.statusCode;
    Map<String,dynamic> body = jsonDecode(response.body);

    Map<String,dynamic> informacionCita = body['data'];

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    if(body['success']==false) {
      respuesta.addAll(
          {
            "success" : false
          }
      );
    } else {

      respuesta.addAll(
          {
            "success" : true,
            "cita" : decodedJSONaCita(informacionCita)
          }
      );
    }

    return respuesta;

  }

  /**
   * Devuelve todos los datos de las citas filtradas del usuario que haga la peticion sea medico o paciente.
   *
   * <br>Puede recibir los siguientes filtros:
   *  * int - id_medico (Para pacientes)
   *  * int - id_paciente (Para médicos)
   *  * String - dia_inicio (Para filtrar citas en el umbral entre dia_inico y dia_fin. Si no hay dia_fin no se tiene en cuenta y viceversa) formato: yyyy-MM-dd
   *  * String - dia_fin
   *  * String - hora (formato hh-mm)
   *  * int - id_usuario (Para demostrar que le pertenece la cita)
   *
   * El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se han devuelto citas o no.
   *    * citas: List<Cita> - solo existirá cuando la petición sea correcta, contiene las filtradas.
   */
  static Future<Map<String,dynamic>> filtrar(Map<String,dynamic>? filtros, int? numPag, int? numRegistros) async {

    List<Cita> listaCitas = [];

    numPag ??= 1;
    numRegistros ??= 5;

    Map<String,dynamic> bodyPeticion = {
      "accion":"filtrar",
      "pag":numPag,
      "numRegistros":numRegistros,
      "id_usuario": Sesion.usuario.id_usuario,
      "jwt":Sesion.usuario.jwt
    };

    if(filtros!=null){
      bodyPeticion.addAll(filtros);
    }

    Response response = await http.post(uri, body: jsonEncode(bodyPeticion));
    int respuesta_http = response.statusCode;

    Map<String,dynamic> body = jsonDecode(response.body);
    Map<String,dynamic> data = body['data'];

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    bool success = body['success'];
    if(success==false) {
      respuesta.addAll(
          {
            "success" : false
          }
      );
    } else if(success) {

      int totalRegistros = data['totalRegistros'];
      data.removeWhere((String key, dynamic value) => key == "totalRegistros");

      // Se rellena la lista con las citas que se han traido.
      data.forEach((key, value) {
        listaCitas.add(decodedJSONaCita(data[key]));
      });

      respuesta.addAll(
          {
            "success" : true,
            "totalRegistros":totalRegistros,
            "citas" : listaCitas
          }
      );
    }

    return respuesta;

  }

  /**
   * Inserta la cita.
   *
   * <br>El objeto tiene que tener indicado los siguientes parametros:
   *  * id_medico
   *  * id_paciente
   *  * hora
   *  * dia
   *
   * El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se ha insertado la cita o no.
   *    * msg: String- Indicará que ha ocurrido con la operación.
   */
  Future<Map<String, dynamic>> insertar() async {

    Map<String,dynamic> body = {"accion":"insertar"};
    body.addAll(aJSON());

    var response = await http.post(uri, body: jsonEncode(body));
    var data = jsonDecode(response.body);

    bool seHaInsertado = data['success'];
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "success": seHaInsertado,
      "msg":msg
    };

    return respuesta;
  }

  /**
   * Modifica la cita.
   *
   * <br>El objeto tiene que tener indicado el id_cita y un parametro cualquiera como mínimo.
   *
   * El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se ha modificado la cita o no.
   *    * msg: String- Indicará que ha ocurrido con la operación.
   */
  Future<Map<String, dynamic>> modificar() async {

    Map<String,dynamic> body = {"accion":"modificar"};
    body.addAll(aJSON());

    var response = await http.post(uri, body: jsonEncode(body));
    var data = jsonDecode(response.body);

    bool seHaModificado = data['success'];
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "modificado": seHaModificado,
      "msg":msg
    };

    return respuesta;
  }

  /**
   * Borra la cita.
   *
   * <br>El objeto tiene que tener indicado el id_cita.
   *
   * El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se ha borrado la cita o no.
   *    * msg: String- Indicará que ha ocurrido con la operación.
   */
  Future<Map<String, dynamic>> borrar() async {

    Map<String,dynamic> body = {
      "accion":"borrar",
      "id_cita": id_cita,
    };
    body.addAll(aJSON());

    var response = await http.post(uri, body: jsonEncode(body));
    var data = jsonDecode(response.body);

    bool seHaBorrado = data['success'];
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "borrado": seHaBorrado,
      "msg":msg
    };

    return respuesta;
  }

  static Future<Map<String, dynamic>> comprobarHorasDisponibles(int id_medico,String dia) async {

    Response response = await http.post(uri, body: jsonEncode({
      "accion" : "comprobarHorasDisponibles",
      "id_medico":id_medico,
      "dia":dia
    }));

    int respuesta_http = response.statusCode;
    Map<String,dynamic> body = jsonDecode(response.body);

    List<String> informacionHorasParse=[];
    List<dynamic> informacionHoras = body['data'];
    informacionHoras.map((hora) => informacionHorasParse.add(hora.toString())).toList();

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    if(body['success']==false) {
      respuesta.addAll(
          {
            "success" : false
          }
      );
    } else {

      respuesta.addAll(
          {
            "success" : true,
            "horas" : informacionHorasParse
          }
      );
    }

    return respuesta;

  }

  @override
  String toString() {
    return "ID_Cita: $id_cita\n"
        "ID_Medico: $id_medico\n"
        "ID_Paciente: $id_paciente\n"
        "ID_Resultado: $id_resultado\n"
        "dia: $dia\n"
        "hora: $hora\n";
  }

}