import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:recursos_aegros/clases/Sesion.dart';
import 'package:recursos_aegros/constantes/Constantes.dart';

class Resultado {

  static final Uri uri = Uri.parse(Constantes.wsResultado);

  int? id_resultado;
  String? tratamiento;
  String? resumen;

  Resultado({
    this.id_resultado,
    this.tratamiento,
    this.resumen,
  });

  static Resultado decodedJSONaResultado(var informacionResultado){

    int? id_resultado = (informacionResultado['id_resultado'] == null ? null : int.parse(informacionResultado['id_resultado']));

    return Resultado(
      id_resultado:id_resultado,
      tratamiento: informacionResultado['tratamiento'],
      resumen: informacionResultado['resumen']
    );
  }

  Map<String,dynamic> aJSON(){
    return {
      "id_resultado":id_resultado,
      "tratamiento":tratamiento,
      "resumen":resumen,

    };
  }

  /**
   * Devuelve todos los datos de un resultado de cita segun el id_resultado. El mapa devuelto contiene tres partes:
   *    * codigo: int - Codigo de respuesta HTTP.
   *    * success: bool - Indica si se ha devuelto un medico o no.
   *    * resultado: Resultado - solo existirá cuando la petición sea correcta, contiene el resultado solicitado.
   */
  static Future<Map<String, dynamic>> buscar(int id_resultado) async{

    Response response = await http.post(uri, body: jsonEncode({
      "accion": "buscar",
      "jwt":Sesion.usuario.jwt,
      "id_resultado":id_resultado
    }));

    int respuesta_http = response.statusCode;
    Map<String,dynamic> body = jsonDecode(response.body);

    Map<String,dynamic> data = body['data'];

    Map<String, dynamic> respuesta =  {
      "codigo": respuesta_http,
    };

    if(body['success']==false) {
      respuesta.addAll(
          {
            "success" : false
          }
      );
    } else {


      respuesta.addAll(
          {
            "success" : true,
            "medico" : decodedJSONaResultado(data)
          }
      );
    }

    return respuesta;

  }
  /**
   * Inserta un resultado para una cita. El mapa devuelto contiene tres partes:
   *  * codigo: int - Codigo de respuesta HTTP.
   *  * msg: String - Resumen de la operación.
   *  * success: bool - Indica si se ha insertado el resultado o no.
   */
  Future<Map<String, dynamic>> insertar(int id_cita) async {

    Map<String,dynamic> body = {
      "accion":"insertar",
      "jwt":Sesion.usuario.jwt,
      "id_cita":id_cita
    };
    body.addAll(aJSON());

    var response = await http.post(uri, body: jsonEncode(body));
    var data = jsonDecode(response.body);

    bool seHaInsertado = data['success'];
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "insertado": seHaInsertado,
      "msg":msg
    };

    return respuesta;
  }

  /**
   * Modifica un resultado para una cita. El mapa devuelto contiene tres partes:
   *  * codigo: int - Codigo de respuesta HTTP.
   *  * msg: String - Resumen de la operación.
   *  * success: bool - Indica si se ha modificado el resultado o no.
   */
  Future<Map<String, dynamic>> modificar() async {

    Map<String,dynamic> body = {
      "accion":"modificar",
      "jwt":Sesion.usuario.jwt
    };
    body.addAll(aJSON());

    var response = await http.post(uri, body: jsonEncode(body));
    var data = jsonDecode(response.body);

    bool seHaModificado = data['success'];
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "modificado": seHaModificado,

      "msg":msg
    };

    return respuesta;
  }

  /**
   * Borra un resultado para una cita. El mapa devuelto contiene tres partes:
   *  * codigo: int - Codigo de respuesta HTTP.
   *  * msg: String - Resumen de la operación.
   *  * success: bool - Indica si se ha borrado el resultado o no.
   */
  Future<Map<String, dynamic>> borrar() async {

    Map<String,dynamic> body = {
      "accion":"borrar",
      "id_resultado": id_resultado,
      "jwt":Sesion.usuario.jwt
    };
    body.addAll(aJSON());

    var response = await http.post(uri, body: jsonEncode(body));
    var data = jsonDecode(response.body);

    bool seHaBorrado = data['success'];
    int respuesta_http = response.statusCode;
    String msg = data['msg'].toString();

    Map<String, dynamic> respuesta =  {
      "codigo":respuesta_http,
      "borrado": seHaBorrado,
      "msg":msg
    };

    return respuesta;
  }

  @override
  String toString() {
    return "ID_Resultado : $id_resultado";
  }


}