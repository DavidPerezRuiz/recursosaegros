import 'package:flutter/cupertino.dart';
import 'package:recursos_aegros/clases/modelos/Clases.dart';

/**
 * Usar siempre este STFUL con todas las pantallas.
 * Comprueba la validez de la sesión cada vez que se crea una pantalla nueva.
 */
abstract class StatefulModificadoState<T extends StatefulWidget> extends State {

  @override
  void initState() {
    Sesion.usuario.comprobarSesionValida();
    super.initState();
  }
}
