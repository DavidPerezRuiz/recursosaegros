import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'modelos/Usuario.dart';
import 'Alerta.dart';

class Sesion {

  static late Usuario usuario;


  /**
   * Cierra la sesión y manda al usuario al login.
   */
  static void cerrarSesion(BuildContext contexto, bool sesionCaducada){
    if(contexto!=null){
      if(sesionCaducada){
        Navigator.pushNamed(contexto, '/', arguments: {"sesionCaducada":true} );
        // ModalRoute.of(context).settings.arguments para recuperar esto en el login
        // si es true hacer pop al login, si no existe mandar a la pagina principal
      }
      else {
        Navigator.pushReplacementNamed(contexto, '/');
      }

    }

  }

  /**
   * Inicia sesión y dependiendo del rol manda a una pagina principal o a otra
   */
  static void iniciarSesion(BuildContext contexto){
    if(contexto!=null){
      if(usuario.rol=="Medico"){
        Navigator.pushReplacementNamed(contexto, 'calen');
      } else if(usuario.rol=="Paciente"){
        Navigator.pushReplacementNamed(contexto, 'PacientesMain');
      } else if(usuario.rol=="Administrador"){
        gestionRespuesta(400, "Los administradores no pueden iniciar sesión desde la aplicación", false, contexto);
      }

    }

  }

  /**
   * Llamar despues de hacer cualquier acción relacionada a los modelos. Se mostrará una alerta
   * con lo que ha ocurrido en la operación.
   *
   * En caso de haber una respuesta
   * 401 se mandara al usuario al login.
   */
  static void gestionRespuesta(int respuesta, String msg, bool success, BuildContext contexto) {

    if(respuesta==401){
      cerrarSesion(contexto, true);
    }

    if(contexto!=null) {
      Alerta.generarAlerta(
          contexto,
          msg,
          success
      );
    }


  }




}