

import 'package:flutter/material.dart';

class Alerta{

  /**
   * Devuelve una alerta con los atributos proporcionados.
   */
  static void generarAlerta(BuildContext contexto, String msg, bool success){

    String titulo = success ? "Acción realizada satisfactoriamente" : "Error al realizar la acción";

    showDialog(context: contexto, builder: (BuildContext context){
      return AlertDialog(
        title: Text(titulo),
        content: Text(msg),
        actions: [
          MaterialButton(
              child: Text("Aceptar"),
              onPressed: () => Navigator.pop(context))
        ],
      );
    });

  }

}