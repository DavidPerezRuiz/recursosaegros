import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Preferencias extends ChangeNotifier{

  // Preferencias
  bool _tema = false;
  bool _recordarSesion = false;

  bool _preferenciasCargadas = false;

  Preferencias(){
    _cargarPreferencias();
  }

  // PREFERENCIAS

  Future<void> _cargarPreferencias() async {
    await SharedPreferences.getInstance().then((prefs) {
      _preferenciasCargadas = true;
      setTema( prefs.getBool("tema") ?? false );
      setRecordarSesion(prefs.getBool("recordarSesion") ?? false );
    });

  }
  Future<void> _guardarPreferencias() async {
    await SharedPreferences.getInstance().then((prefs) {
      prefs.setBool("tema", _tema);
      prefs.setBool("recordarSesion", _recordarSesion);
    });
  }
  get estanPreferenciasCargadas{
    return _preferenciasCargadas;
  }

  bool getTema(){
    return _tema;
  }
  void setTema(bool newValue){
    _tema = newValue;
    _guardarPreferencias();
    notifyListeners();
  }

  bool getRecordarSesion(){
    return _recordarSesion;
  }
  void setRecordarSesion(bool newValue){
    _recordarSesion = newValue;
    _guardarPreferencias();
    notifyListeners();
  }


}


/*
class Preferencias{
  final bool estaTemaOscuroActivado;
  const Preferencias({this.estaTemaOscuroActivado=false});
}

class PreferenciasNotificador extends ChangeNotifier{

  Preferencias _prefs = Preferencias(estaTemaOscuroActivado: false);

  PreferenciasNotificador(){
    _cargarPreferencias();
  }

  /**
   * Coge las preferencias guardadas y crea un objeto preferencias con ellas.
   * Si no tiene preferencias guardadas aun se les da una por defecto.
   */
  Future<void> _cargarPreferencias() async{
    await SharedPreferences.getInstance().then((sPrefs) {
      bool estaTemaOscuroActivado = sPrefs.getBool("estaTemaOscuroActivado") ?? false;
      _prefs = Preferencias(estaTemaOscuroActivado: estaTemaOscuroActivado);

      notifyListeners();
    });
  }
  /**
   * Guarda el estado actual de las preferencias.
   */
  Future<void> _guardarPreferencias() async {
    await SharedPreferences.getInstance().then((sPrefs) {
      sPrefs.setBool("estaTemaOscuroActivado", _prefs.estaTemaOscuroActivado);
    });
  }

  bool get estaTemaOscuroActivado{
    return _prefs.estaTemaOscuroActivado;
  }

  set activarTemaOscuro(bool activarTemaOscuro){
    _prefs = Preferencias(estaTemaOscuroActivado: activarTemaOscuro);
    notifyListeners();
    _guardarPreferencias();

  }

}*/